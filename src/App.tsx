import "./assets/styles/style.scss";
import "bootstrap/dist/js/bootstrap";
import React, { useState } from "react";
import CardList from "./components/CardList";
import AddCardForm from "./components/AddCardForm";
import EditCardForm from "./components/EditCardForm";
import { Card } from "./interfaces/interfaces";
import Modal from "./components/shared/Modal";

const App: React.FC = () => {
  const initialCards: Card[] = [
    {
      cardNumber: "1234567890123456",
      cardHolderName: "John Doe",
      expirationDate: "12/23",
      ccv: "123",
    },
    {
      cardNumber: "9876543210987654",
      cardHolderName: "Jane Smith",
      expirationDate: "05/22",
      ccv: "456",
    },
  ];
  const [cards, setCards] = useState<Card[]>(initialCards);

  // State variables for modals
  const [showAddCardModal, setShowAddCardModal] = useState<boolean>(false);
  const [showEditCardModal, setShowEditCardModal] = useState<boolean>(false);
  const [selectedCard, setSelectedCard] = useState<Card | null>(null);

  const handleAddCard = (newCard: Card) => {
    setCards([...cards, newCard]);
    setShowAddCardModal(false);
  };

  const handleEditCard = (ccv: string, updatedCard: Card) => {
    const updatedCards = cards.map((card) =>
      card.ccv === ccv ? { ...card, ...updatedCard } : card
    );
    setCards(updatedCards);
    setShowEditCardModal(false);
  };

  const handleDeleteCard = () => {
    if (selectedCard) {
      const updatedCards = cards.filter(
        (card) => card.ccv !== selectedCard.ccv
      );
      setCards(updatedCards);
      closeModals();
    }
  };

  const openEditCardModal = (card: Card) => {
    setSelectedCard(card);
    setShowEditCardModal(true);
  };

  const closeModals = () => {
    setShowAddCardModal(false);
    setShowEditCardModal(false);
    setSelectedCard(null);
  };

  return (
    <React.Fragment>
      <div className="container-fluid d-flex justify-content-center vh-100 p-0">
        <div className="main-container px-4 d-flex flex-column justify-content-between">
          <div>
            <h1
              data-testid="App title"
              className="fw-bold text-cosumo-light-purple mt-4"
            >
              Your cards
            </h1>
            <p>Add, edit or delete your cards any time</p>
            <CardList cards={cards} onEditCard={openEditCardModal} />
          </div>
          <div className="sticky-bottom py-4 m-0 d-flex justify-content-center">
            <button
              className="custom-button w-100 fs-5"
              onClick={() => setShowAddCardModal(true)}
            >
              Add new card
            </button>
          </div>
        </div>
      </div>
      <Modal show={showAddCardModal} onClose={closeModals}>
        <AddCardForm onAddCard={handleAddCard} />
      </Modal>

      <Modal show={showEditCardModal} onClose={closeModals}>
        <EditCardForm
          card={selectedCard}
          onUpdateCard={handleEditCard}
          onDeleteCard={handleDeleteCard}
        />
      </Modal>
    </React.Fragment>
  );
};

export default App;
