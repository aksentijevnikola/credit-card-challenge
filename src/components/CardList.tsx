import React from "react";
import Card from "./shared/Card";
import { CardListProps } from "../interfaces/interfaces";

const CardList: React.FC<CardListProps> = ({ cards, onEditCard }) => {
  return (
    <div className="row">
      <div className="col-12 d-flex flex-column">
        {cards.map((card) => (
          <div key={card.ccv}>
            <Card card={card} onEdit={() => onEditCard(card)} />
          </div>
        ))}
      </div>
    </div>
  );
};

export default CardList;
