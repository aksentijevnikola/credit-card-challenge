import React, { useState, useEffect } from "react";
import { validateCardDetails } from "../utils/validation";
import {
  Card as CardInterface,
  ValidationErrors,
} from "../interfaces/interfaces";
import Card from "./shared/Card";

interface EditCardFormProps {
  card: CardInterface | null;
  onUpdateCard: (ccv: string, updatedCard: CardInterface) => void;
  onDeleteCard: () => void;
}

const EditCardForm: React.FC<EditCardFormProps> = ({
  card,
  onUpdateCard,
  onDeleteCard,
}) => {
  const [cardDetails, setCardDetails] = useState<CardInterface>({
    cardNumber: "",
    cardHolderName: "",
    expirationDate: "",
    ccv: "",
  });

  const [errors, setErrors] = useState<ValidationErrors>({});
  const [submitted, setSubmitted] = useState<boolean>(false);

  useEffect(() => {
    if (card) {
      setCardDetails({
        cardNumber: card.cardNumber,
        cardHolderName: card.cardHolderName,
        expirationDate: card.expirationDate,
        ccv: card.ccv,
      });
    }
  }, [card]);

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    // Update form state
    setCardDetails({
      ...cardDetails,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    setSubmitted(true);
    // Validate form inputs
    const validationErrors = validateCardDetails(cardDetails);

    if (Object.keys(validationErrors).length === 0 && card) {
      // If no validation errors, submit the form
      onUpdateCard(card.ccv, cardDetails);

      setCardDetails({
        cardNumber: "",
        cardHolderName: "",
        expirationDate: "",
        ccv: "",
      });
      setErrors({});
    } else {
      setErrors(validationErrors);
    }
  };
  const inputClass = (fieldName: keyof ValidationErrors) => {
    if (submitted) {
      return errors[fieldName] ? "error" : "success";
    }
    return "";
  };

  return (
    <form onSubmit={handleSubmit}>
      <h2 className="fw-bold fs-4">Edit your card</h2>
      <Card card={cardDetails} />
      <div>
        <label htmlFor="cardHolderName">Name on Card:</label>
        <input
          id="cardHolderName"
          type="text"
          name="cardHolderName"
          className={inputClass("cardHolderName")}
          value={cardDetails.cardHolderName}
          onChange={handleInputChange}
        />
        {errors.cardHolderName && (
          <span className="error">{errors.cardHolderName}</span>
        )}
      </div>

      <div>
        <label htmlFor="cardNumber">Card Number:</label>
        <input
          id="cardNumber"
          type="text"
          name="cardNumber"
          className={inputClass("cardNumber")}
          value={cardDetails.cardNumber}
          onChange={handleInputChange}
        />
        {errors.cardNumber && (
          <span className="error">{errors.cardNumber}</span>
        )}
      </div>

      <div>
        <label htmlFor="expirationDate">Expiry Date:</label>
        <input
          id="expirationDate"
          type="text"
          name="expirationDate"
          className={inputClass("expirationDate")}
          value={cardDetails.expirationDate}
          onChange={handleInputChange}
        />
        {errors.expirationDate && (
          <span className="error">{errors.expirationDate}</span>
        )}
      </div>

      <div>
        <label htmlFor="ccv">CCV (Security Code):</label>
        <input
          id="ccv"
          type="text"
          name="ccv"
          className={inputClass("ccv")}
          value={cardDetails.ccv}
          onChange={handleInputChange}
        />
        {errors.ccv && <span className="error">{errors.ccv}</span>}
      </div>

      <button type="submit">Confirm</button>
      <button
        type="button"
        onClick={onDeleteCard}
        className="bg-transparent text-cosumo-light-gray"
      >
        Delete card
      </button>
    </form>
  );
};

export default EditCardForm;
