import React from "react";
import { Card as CardType } from "../../interfaces/interfaces";
import mastercardLogo from "../../assets/svgs/mastercard-logo.svg";
import editCardIcon from "../../assets/svgs/edit-icon.svg";

interface CardProps {
  card: CardType;
  onEdit?: () => void;
}

const Card: React.FC<CardProps> = ({ card, onEdit }) => {
  const formatCardNumber = (cardNumber: string) => {
    const chunks = cardNumber.match(/.{1,4}/g); // Split into chunks of 4 characters
    return chunks ? chunks.join(" ") : ""; // Join them with a space between them
  };

  return (
    <div className="card-container d-flex flex-column justify-content-between text-white p-4 my-3">
      <div className="d-flex flex-row justify-content-between">
        <img src={mastercardLogo} alt="Mastercard Logo" />
        <div className="d-flex flex-row">
          <div className="d-flex flex-column text-end me-4">
            <span>CCV</span> <span className="fw-bold fs-5">{card.ccv}</span>
          </div>
          <div className="d-flex flex-column text-end">
            <span>Expires</span>
            <span className="fw-bold fs-5">{card.expirationDate}</span>
          </div>
        </div>
      </div>
      <div className="d-flex flex-row justify-content-between">
        <div>
          <div>
            <span className="fw-bold fs-5">{card.cardHolderName}</span>
          </div>
          <div>{formatCardNumber(card.cardNumber)}</div>
        </div>
        {onEdit && (
          <button
            data-testid={`card-edit-${card.cardNumber}`}
            onClick={onEdit}
            className="bg-transparent"
          >
            <img src={editCardIcon} alt="Edit Icon" />
          </button>
        )}
      </div>
    </div>
  );
};

export default Card;
