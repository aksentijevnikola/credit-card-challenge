import React, { ReactNode } from "react";

interface ModalProps {
  show: boolean;
  onClose: () => void;
  children: ReactNode;
}

const Modal: React.FC<ModalProps> = ({ show, onClose, children }) => {
  const modalClasses = `modal-wrapper ${show ? "show" : ""}`;

  return (
    <>
      {show && (
        <div className={modalClasses}>
          <div className="modal-content">
            <div className="fs-3 d-flex justify-content-end">
              <span onClick={onClose} className="cursor-pointer">
                &times;
              </span>
            </div>
            {children}
          </div>
        </div>
      )}
    </>
  );
};

export default Modal;
