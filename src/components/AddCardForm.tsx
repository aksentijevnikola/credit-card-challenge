import React, { useState } from "react";
import { validateCardDetails } from "../utils/validation";
import { Card, ValidationErrors } from "../interfaces/interfaces";

interface AddCardFormProps {
  onAddCard: (newCard: Card) => void;
}

const AddCardForm: React.FC<AddCardFormProps> = ({ onAddCard }) => {
  const [cardDetails, setCardDetails] = useState({
    cardNumber: "",
    cardHolderName: "",
    expirationDate: "",
    ccv: "",
  });

  const [errors, setErrors] = useState<ValidationErrors>({});
  const [submitted, setSubmitted] = useState<boolean>(false);

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setCardDetails({
      ...cardDetails,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    setSubmitted(true);

    const validationErrors = validateCardDetails(cardDetails);

    if (Object.keys(validationErrors).length === 0) {
      onAddCard(cardDetails);
      setCardDetails({
        cardNumber: "",
        cardHolderName: "",
        expirationDate: "",
        ccv: "",
      });
      setErrors({});
    } else {
      setErrors(validationErrors);
    }
  };

  const inputClass = (fieldName: keyof ValidationErrors) => {
    // Track if submit button has been clicked to set the error or success classes to inputs
    if (submitted) {
      return errors[fieldName] ? "error" : "success";
    }
    return "";
  };

  return (
    <form onSubmit={handleSubmit}>
      <h2 className="fw-bold fs-4 mb-5">Add your card details</h2>

      <div>
        <label htmlFor="cardHolderName">Name on Card:</label>
        <input
          id="cardHolderName"
          type="text"
          name="cardHolderName"
          className={inputClass("cardHolderName")}
          value={cardDetails.cardHolderName}
          onChange={handleInputChange}
        />
        {errors.cardHolderName && (
          <span className="error">{errors.cardHolderName}</span>
        )}
      </div>

      <div>
        <label htmlFor="cardNumber">Card Number:</label>
        <input
          id="cardNumber"
          type="text"
          name="cardNumber"
          className={inputClass("cardNumber")}
          value={cardDetails.cardNumber}
          onChange={handleInputChange}
        />
        {errors.cardNumber && (
          <span className="error">{errors.cardNumber}</span>
        )}
      </div>

      <div>
        <label htmlFor="expirationDate">Expiry Date:</label>
        <input
          id="expirationDate"
          type="text"
          name="expirationDate"
          className={inputClass("expirationDate")}
          value={cardDetails.expirationDate}
          onChange={handleInputChange}
        />
        {errors.expirationDate && (
          <span className="error">{errors.expirationDate}</span>
        )}
      </div>

      <div>
        <label htmlFor="ccv">CCV (Security Code):</label>
        <input
          id="ccv"
          type="text"
          name="ccv"
          className={inputClass("ccv")}
          value={cardDetails.ccv}
          onChange={handleInputChange}
        />
        {errors.ccv && <span className="error">{errors.ccv}</span>}
      </div>

      <button type="submit">Confirm</button>
    </form>
  );
};

export default AddCardForm;
