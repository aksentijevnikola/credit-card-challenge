import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import Card from "../shared/Card";

const mockCard = {
  cardNumber: "1234567890123456",
  cardHolderName: "John Doe",
  expirationDate: "12/23",
  ccv: "123",
};

describe("Card component", () => {
  it("renders card details correctly", () => {
    render(<Card card={mockCard} />);

    // Check if card details are rendered correctly
    expect(screen.getByAltText("Mastercard Logo")).toBeInTheDocument();
    expect(screen.getByText("CCV")).toBeInTheDocument();
    expect(screen.getByText("Expires")).toBeInTheDocument();
    expect(screen.getByText("John Doe")).toBeInTheDocument();
    expect(screen.getByText("1234 5678 9012 3456")).toBeInTheDocument();
  });

  it("renders edit button when onEdit is provided", () => {
    const handleEdit = jest.fn();
    render(<Card card={mockCard} onEdit={handleEdit} />);

    // Check if the edit button is rendered
    const editButton = screen.getByAltText("Edit Icon");
    expect(editButton).toBeInTheDocument();

    // Simulate click on the edit button
    fireEvent.click(editButton);

    // Check if the onEdit callback is called
    expect(handleEdit).toHaveBeenCalled();
  });

  it("does not render edit button when onEdit is not provided", () => {
    render(<Card card={mockCard} />);

    // Check if the edit button is not rendered
    expect(screen.queryByAltText("Edit Icon")).not.toBeInTheDocument();
  });
});
