import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import CardList from "../CardList";
import { Card } from "../../interfaces/interfaces";

const initialCards: Card[] = [
  {
    cardNumber: "1234567890123456",
    cardHolderName: "John Doe",
    expirationDate: "12/23",
    ccv: "123",
  },
  {
    cardNumber: "9876543210987654",
    cardHolderName: "Jane Smith",
    expirationDate: "05/22",
    ccv: "456",
  },
];

const formatCardNumber = (cardNumber: string) => {
  const chunks = cardNumber.match(/.{1,4}/g);
  return chunks ? chunks.join(" ") : "";
};

describe("CardList component", () => {
  const mockOnEditCard = jest.fn();

  it("renders the initial cards", () => {
    render(<CardList cards={initialCards} onEditCard={mockOnEditCard} />);

    initialCards.forEach((card) => {
      expect(screen.getByText(card.cardHolderName)).toBeInTheDocument();
      const formattedCardNumber = formatCardNumber(card.cardNumber);
      expect(screen.getByText(formattedCardNumber)).toBeInTheDocument();
    });
  });

  it("triggers onEditCard when edit button is clicked", () => {
    render(<CardList cards={initialCards} onEditCard={mockOnEditCard} />);

    initialCards.forEach((card) => {
      const editButton = screen.getByTestId(`card-edit-${card.cardNumber}`);
      fireEvent.click(editButton);

      expect(mockOnEditCard).toHaveBeenCalledWith(card);
    });
  });
});
