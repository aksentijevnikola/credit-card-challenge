import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import AddCardForm from "../AddCardForm";

const mockOnAddCard = jest.fn();

describe("AddCardForm component", () => {
  it("renders form and input fields", () => {
    render(<AddCardForm onAddCard={mockOnAddCard} />);

    expect(screen.getByText("Add your card details")).toBeInTheDocument();
    expect(screen.getByLabelText("Name on Card:")).toBeInTheDocument();
    expect(screen.getByLabelText("Card Number:")).toBeInTheDocument();
    expect(screen.getByLabelText("Expiry Date:")).toBeInTheDocument();
    expect(screen.getByLabelText("CCV (Security Code):")).toBeInTheDocument();
    expect(screen.getByText("Confirm")).toBeInTheDocument();
  });

  it("updates input values on change", () => {
    render(<AddCardForm onAddCard={mockOnAddCard} />);

    // Simulate input changes
    fireEvent.change(
      screen.getByLabelText("Name on Card:") as HTMLInputElement,
      {
        target: { value: "John Doe" },
      }
    );
    fireEvent.change(
      screen.getByLabelText("Card Number:") as HTMLInputElement,
      {
        target: { value: "1234567890123456" },
      }
    );
    fireEvent.change(
      screen.getByLabelText("Expiry Date:") as HTMLInputElement,
      {
        target: { value: "12/23" },
      }
    );
    fireEvent.change(
      screen.getByLabelText("CCV (Security Code):") as HTMLInputElement,
      {
        target: { value: "123" },
      }
    );

    // Check if input values are updated
    expect(
      (screen.getByLabelText("Name on Card:") as HTMLInputElement).value
    ).toBe("John Doe");
    expect(
      (screen.getByLabelText("Card Number:") as HTMLInputElement).value
    ).toBe("1234567890123456");
    expect(
      (screen.getByLabelText("Expiry Date:") as HTMLInputElement).value
    ).toBe("12/23");
    expect(
      (screen.getByLabelText("CCV (Security Code):") as HTMLInputElement).value
    ).toBe("123");
  });
});
