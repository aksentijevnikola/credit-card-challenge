import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import EditCardForm from "../EditCardForm";

// Mock function for onUpdateCard and onDeleteCard
const mockOnUpdateCard = jest.fn();
const mockOnDeleteCard = jest.fn();

// Mock card data
const mockCard = {
  cardNumber: "1234567890123456",
  cardHolderName: "John Doe",
  expirationDate: "12/23",
  ccv: "123",
};

const formatCardNumber = (cardNumber: string) => {
  const chunks = cardNumber.match(/.{1,4}/g);
  return chunks ? chunks.join(" ") : "";
};

describe("EditCardForm component", () => {
  it("renders the EditCardForm with initial card details", () => {
    render(
      <EditCardForm
        card={mockCard}
        onUpdateCard={mockOnUpdateCard}
        onDeleteCard={mockOnDeleteCard}
      />
    );

    // Check if initial card details are rendered
    expect(screen.getByText("Edit your card")).toBeInTheDocument();
    expect(screen.getByText(mockCard.cardHolderName)).toBeInTheDocument();
    expect(
      screen.getByText(formatCardNumber(mockCard.cardNumber))
    ).toBeInTheDocument();
    expect(screen.getByText(mockCard.expirationDate)).toBeInTheDocument();
    expect(screen.getByText("CCV (Security Code):")).toBeInTheDocument();
    expect(screen.getByDisplayValue(mockCard.ccv)).toBeInTheDocument();
  });

  it("updates input values on change", () => {
    render(
      <EditCardForm
        card={mockCard}
        onUpdateCard={mockOnUpdateCard}
        onDeleteCard={mockOnDeleteCard}
      />
    );

    // Simulate input changes
    fireEvent.change(screen.getByLabelText("Name on Card:"), {
      target: { value: "Jane Smith" },
    });
    fireEvent.change(screen.getByLabelText("Card Number:"), {
      target: { value: "9876543210987654" },
    });
    fireEvent.change(screen.getByLabelText("Expiry Date:"), {
      target: { value: "05/25" },
    });
    fireEvent.change(screen.getByLabelText("CCV (Security Code):"), {
      target: { value: "456" },
    });

    // Check if input values are updated
    expect(
      (screen.getByLabelText("Name on Card:") as HTMLInputElement).value
    ).toBe("Jane Smith");
    expect(
      (screen.getByLabelText("Card Number:") as HTMLInputElement).value
    ).toBe("9876543210987654");
    expect(
      (screen.getByLabelText("Expiry Date:") as HTMLInputElement).value
    ).toBe("05/25");
    expect(
      (screen.getByLabelText("CCV (Security Code):") as HTMLInputElement).value
    ).toBe("456");
  });

  it("submits form with valid inputs", () => {
    render(
      <EditCardForm
        card={mockCard}
        onUpdateCard={mockOnUpdateCard}
        onDeleteCard={mockOnDeleteCard}
      />
    );

    // Simulate input changes
    fireEvent.change(screen.getByLabelText("Name on Card:"), {
      target: { value: "Jane Smith" },
    });
    fireEvent.change(screen.getByLabelText("Card Number:"), {
      target: { value: "9876543210987654" },
    });
    fireEvent.change(screen.getByLabelText("Expiry Date:"), {
      target: { value: "05/25" },
    });
    fireEvent.change(screen.getByLabelText("CCV (Security Code):"), {
      target: { value: "456" },
    });

    // Simulate form submission
    fireEvent.submit(screen.getByText("Confirm"));

    // Check if onUpdateCard is called with the updated card details
    expect(mockOnUpdateCard).toHaveBeenCalledWith(mockCard.ccv, {
      cardNumber: "9876543210987654",
      cardHolderName: "Jane Smith",
      expirationDate: "05/25",
      ccv: "456",
    });
  });

  it("displays validation errors for invalid inputs", () => {
    render(
      <EditCardForm
        card={mockCard}
        onUpdateCard={mockOnUpdateCard}
        onDeleteCard={mockOnDeleteCard}
      />
    );

    // Simulate input changes
    fireEvent.change(screen.getByLabelText("Name on Card:"), {
      target: { value: "J" },
    });
    fireEvent.change(screen.getByLabelText("Card Number:"), {
      target: { value: "9876543210987" },
    });
    fireEvent.change(screen.getByLabelText("Expiry Date:"), {
      target: { value: "" },
    });
    fireEvent.change(screen.getByLabelText("CCV (Security Code):"), {
      target: { value: "45" },
    });
    // Simulate form submission without filling required fields
    fireEvent.submit(screen.getByText("Confirm"));

    // Check if validation errors are displayed
    expect(
      screen.getByText(
        "Invalid card holder name. Must include both first and last names."
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText("Invalid card number. Must be a 16-digit number.")
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        "Invalid expiration date. Must be in the format MM/YY and not expired."
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText("Invalid CCV. Must be a 3 or 4-digit number.")
    ).toBeInTheDocument();
  });

  it("calls onDeleteCard when 'Delete card' button is clicked", () => {
    render(
      <EditCardForm
        card={mockCard}
        onUpdateCard={mockOnUpdateCard}
        onDeleteCard={mockOnDeleteCard}
      />
    );

    // Simulate clicking the 'Delete card' button
    fireEvent.click(screen.getByText("Delete card"));

    // Check if onDeleteCard is called
    expect(mockOnDeleteCard).toHaveBeenCalled();
  });
});
