import { Card, ValidationErrors } from "../interfaces/interfaces";

export const validateCardDetails = (cardDetails: Card): ValidationErrors => {
  const errors: ValidationErrors = {};

  // Validation logic for card number
  if (!cardDetails.cardNumber || !/^\d{16}$/.test(cardDetails.cardNumber)) {
    errors.cardNumber = "Invalid card number. Must be a 16-digit number.";
  }

  // Validation logic for card holder name
  if (
    !cardDetails.cardHolderName ||
    !/^[a-zA-Z]+ [a-zA-Z]+$/.test(cardDetails.cardHolderName)
  ) {
    errors.cardHolderName =
      "Invalid card holder name. Must include both first and last names.";
  }

  // Validation logic for expiration date
  if (
    !cardDetails.expirationDate ||
    !isValidExpirationDate(cardDetails.expirationDate)
  ) {
    errors.expirationDate =
      "Invalid expiration date. Must be in the format MM/YY and not expired.";
  }

  // Validation logic for CCV
  if (!cardDetails.ccv || !/^\d{3,4}$/.test(cardDetails.ccv)) {
    errors.ccv = "Invalid CCV. Must be a 3 or 4-digit number.";
  }

  return errors;
};

const isValidExpirationDate = (expirationDate: string): boolean => {
  const currentDate = new Date();
  const currentYear = currentDate.getFullYear() % 100; // Extract last two digits of the year
  const currentMonth = currentDate.getMonth() + 1;

  const [inputMonth, inputYear] = expirationDate.split("/").map(Number);

  if (
    inputYear < currentYear ||
    (inputYear === currentYear && inputMonth < currentMonth)
  ) {
    // Expiration date is in the past
    return false;
  }

  // Expiration date is valid
  return true;
};
