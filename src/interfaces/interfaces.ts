export interface Card {
  cardNumber: string;
  cardHolderName: string;
  expirationDate: string;
  ccv: string;
}

export interface CardProps {
  card: Card;
  onEdit: () => void;
}

export interface CardListProps {
  cards: Card[];
  onEditCard: (card: Card) => void;
}

export interface AddCardFormProps {
  showModal: boolean;
  onClose: () => void;
  onAddCard: (newCard: Card) => void;
}

export interface AddCardFormState {
  cardNumber: string;
  cardHolderName: string;
  expirationDate: string;
  ccv: string;
}

export interface ValidationErrors {
  cardNumber?: string;
  cardHolderName?: string;
  expirationDate?: string;
  ccv?: string;
}
