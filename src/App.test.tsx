import React from "react";
import { render, screen, fireEvent, waitFor } from "@testing-library/react";
import App from "./App";

describe("App component", () => {
  test("renders 'Your cards' header", () => {
    render(<App />);
    expect(screen.getByTestId(/App title/i)).toBeInTheDocument();
  });

  test("opens 'Add Card' modal when 'Add new card' button is clicked", () => {
    render(<App />);
    fireEvent.click(screen.getByText(/add new card/i));
    // make sure the 'Add Card' modal is rendered
    expect(screen.getByText(/add your card details/i)).toBeInTheDocument();
  });

  test("closes modal when 'Close Modal' button is clicked", () => {
    render(<App />);
    fireEvent.click(screen.getByText(/add new card/i));

    // Simulate close button click
    fireEvent.click(screen.getByText("×"));

    // Make sure the modal is closed
    expect(
      screen.queryByText(/add your card details/i)
    ).not.toBeInTheDocument();
  });
  test("adds a new card and closes 'Add Card' modal", async () => {
    render(<App />);
    fireEvent.click(screen.getByText(/Add new card/i));

    // Simulate adding a new card
    fireEvent.change(screen.getByLabelText(/Name on Card:/i), {
      target: { value: "Nikola Aksentijev" },
    });
    fireEvent.change(screen.getByLabelText(/Card Number:/i), {
      target: { value: "1234567890123456" },
    });
    fireEvent.change(screen.getByLabelText(/Expiry Date:/i), {
      target: { value: "12/26" },
    });
    fireEvent.change(screen.getByLabelText(/CCV \(security code\)/i), {
      target: { value: "881" },
    });

    fireEvent.click(screen.getByText(/confirm/i));

    // Use waitFor to wait for the DOM to update
    await waitFor(() => {
      // Make sure the 'Add Card' modal is closed
      expect(
        screen.queryByText(/add your card details/i)
      ).not.toBeInTheDocument();
    });

    // Ensure the new card is added
    expect(screen.getByText(/Nikola Aksentijev/i)).toBeInTheDocument();
  });
});
